"use strict";
function count(input) {
    return input.length
}
console.log(count('Jonathan Robles'))

//15


//``````````````````````````````ANONYMOUS FUNCTIONS FOR ADDITION```````````````````````
var increment = function add(a, b) {
    return a + b;
};
var two = increment(1, 1);
console.log(two);

//2



//``````````````````````````````NON ANONYMOUS FUNCTION FOR ADDITION
function add(a, b) {
    return a + b;
}
console.log(add(3, 5));

//8


//``````````````````````````````ANONYMOUS FUNCTIONS FOR SUBTRACTION```````````````````````
var sub2 = function subtract(a, b) {
    return a - b;
};
var four1 = sub2(8,4);
console.log(four1);

//4



//```````````````````````````````Non Anonymous Function FOR SUBTRACTION````````````````````
function subtract(a, b) {
    return a - b;
}
console.log(subtract(3,2))

//1




//``````````````````````````````ANONYMOUS FUNCTIONS FOR MULTIPLICATION```````````````````````
var multiply2 = function multiply(a, b) {
    return a * b;
};
var nine = multiply2(9,1);
console.log(nine);


//9


//```````````````````````````````Non Anonymous Function FOR MULTIPLICATION````````````````````
function multiply(a, b) {
    return a * b;
}
console.log(multiply(3,2))

//6



//```````````````````````````ANONYMOUS FUNCTIONS FOR DIVISION`````````````````````````````
var div2 = function divide(a, b) {
    return a / b;
};
var twenty = div2(60,3);
console.log(twenty);

//20


//```````````````````````````````Non Anonymous Function FOR DIVISION````````````````````
function divide(a, b) {
    return a / b;
}
console.log(divide(100,2))

//50



//`````````````````````````````ANONYMOUS FUNCTIONS REMAINDER```````````````````````````````````
var remainder1 = function remainder(a, b) {
    return a % b;
};
var four = remainder1(400,6);
console.log(four);

//4



//```````````````````````````````Non Anonymous Function FOR REMAINDER````````````````````
function remainder(number, divisor) {
    return number % divisor
}
console.log(remainder(10,3))

1




//`````````````````````````````````ANONYMOUS FUNCTIONS FOR SQUARE````````````````````````
var square1 = function square(a) {
    return a * a;
};
//CALL AN ANONYMOUS FUNCTIONS
var square12 = square(8);
console.log(square12);

//64


//```````````````````````````````Non Anonymous Function FOR SQUARE````````````````````
function square(a) {
    return a * a;
}
console.log(square(3))

//9


