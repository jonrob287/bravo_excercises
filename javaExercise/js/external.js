//ALWAYS PUT IN "USE STRICT" //helps us to create/write cleaner code
"use strict";
//CONSOLE LOG WELCOME
console.log('Hello from JavaScript External');
//USER INTERACTION
alert('Welcome to my website!');

var userInput = prompt('What is your favorite color?');
console.log('The user entered: ' + userInput);

alert(`Awesome! My favorite color is ${userInput} too!!`);