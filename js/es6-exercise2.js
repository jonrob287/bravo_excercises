const petName = 'Chula';
const age = 14;
// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
console.log("My dog is named " + petName + ' and she is ' + age + ' years old.');
console.log(`My dog is named ${petName} and she is ${age} years old.`);



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }
const addTen = num1 => num1 + 10;
console.log(addTen(490));



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }
const minusFive = num1 => num1 - 5;
console.log(minusFive(4));



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }
const multiplyByTwo = num1 => num1 * 2;
console.log(multiplyByTwo(4));



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };
const greetings = name => 'Hello, ' + name + ' how are you?';
console.log(greetings('Bob'));



// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }
const haveWeMet = name => (name === 'Bob') ? 'Nice to see you again' : 'Nice to meet you';
console.log(haveWeMet('Bob'));
console.log(haveWeMet('Jon'));

// TERNARY OPERATOR
// syntanx
//condition ? message a : message b







