"use strict";
// MAP FILTER REDUCE EXERCISE


const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearsOfExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearsOfExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearsOfExperience: 2
    },
    {
        name: 'faith',
        email: 'faith@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearsOfExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearsOfExperience: 8
    }
];


/**
 *          stephen@appddictionstudio.com       longest email
 *          karen@appddictionstudio.com
 *          juan@appddictionstudio.com
 *          faith@codebound.com
 *          dwight@codebound.com
 */

/**
    1.  Use .filter to create an array of developer objects where they have
        at least 5 languages in the languages array

        Need to use .filter thru developers and create an array and if developer object has 5 or more languages push to the new array

 */
// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
//
// var evens = numbers.filter(function (n) {
//     return n % 2 === 0;
// });
// console.log(evens)
//
// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// const evens = numbers.filter(n => n % 2 === 0);
// console.log(evens)

const moreThanFiveLanguages = developers.filter( developer => developer.languages.length >= 5);
console.log(moreThanFiveLanguages)


/**
     2. Use .map to create an array of strings where each element is a developer's
        email address

        Need to use .map to create an array of strings from each email field of each developer object

 */

// MAP
// var increment = numbers.map(function (num) {
//     return num + 1;
// })
// console.log(increment)

//~~~~~~~~~~~~~~~~~~~~~~~~~~ES6
// const increment = numbers.map(num => num +1)
// console.log(increment)

const emails = developers.map(developer => developer.email)
console.log(emails)




/**
     3. Use reduce to get the total years of experience from the list of developers.
        Once you get the total of years you can use the result to calculate the average.
 */
// .reduce =used to ' reduce' a collection to a single value.
// const nums = [1, 2, 3, 4, 5];
// const sum = nums.reduce((accumulation, currentNumber)=>{
//     return accumulation + currentNumber
// }, 0);
// console.log(sum);
//
// const officeSales = [
//     {
//         name: 'Jim Halpert',
//         sales: 500
//     },
//     {
//         name: 'Dwight Schrute',
//         sales: 750
//     },
//     {
//         name: 'Ryan Howard',
//         sales: 150
//     }
// ];
//
// const totalSales = officeSales.reduce((totals, person)=>{
//     return totals + person.sales;
// }, 0);
// console.log(totalSales);


const totalYearsExperience = developers.reduce((totalExperience, developer) => {
        return totalExperience + developer.yearsOfExperience;
}, 0);
const average = totalYearsExperience / developers.length;

console.log(totalYearsExperience)
console.log(average)


/**
 * 4.   Use reduce to get the longest email from the list.*/
// var arr = [
//     {
//         "title": "Ender's Game",
//         "genre": "Science Fiction",
//         "year": 2008
//     },
//     {
//         "title": "Harry Potter and the Goblet of Fire",
//         "genre": "fantasy",
//         "year": 2002
//     },
//     {
//         "title": "Pride and Predjudice",
//         "genre": "romance",
//         "year": 1980
//     },
//     {
//         "title": "Pride and Predjudice",
//         "genre": "asdasdasd asdasdasdsa asdasda",
//         "year": 1980
//     }
// ]
// var longest = arr.reduce(function (a, b) { return a.genre.length > b.genre.length ? a : b; }).genre.length;
// console.log(longest);

const longestEmail = developers.reduce((a, b) => {return a.email.length > b.email.length ? a : b; }).email;
console.log(longestEmail)

/**
 * 5.   Use reduce to get the list of developer's names in a single string
 -      output:
        CodeBound Staff: stephen, karen, juan, faith, dwight*/
// var data = [ {name: 'Bart'}, {name: 'Lisa'}, {name: 'Maggie'} ]
// var result = function(){
//     var names = data.reduce(function(a, b){
//             return (a.name || a) + ',' + b.name
//     })
//     return names
// };
// var res = result();
// console.log(res)

const listOfDeveloperNames = developers.reduce(function(name, staff)
{
        return name + staff.name + ', ';

}, 'CodeBound Staff: ');
console.log(listOfDeveloperNames)

/**
 *      BONUS: use reduce to get the unique list of languages from the list
        of developers*/






