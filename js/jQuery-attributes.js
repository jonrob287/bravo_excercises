'use strict';
//ATTRIBUTES
/*
    .html()
    .css()
    .addClass() - adds the specified class(es) to each of the set of matched elements
    .removeClass() - removes a single class, multiple classes or all classes
    .toggleClass() - add or remove one or more classes.


    GETTERS/SETTERS

    METHOD CHAINING
 */
// $('#name').css('color', 'red').css('background-color', 'cyan');
//
//~~~~~~~~~ OR ~~~~~~~~~~~~
//
// var highlightStyling ={
//     'color': 'red',
//     'background-color': 'blue',
//     'font-size': '30px'
// }
// $('#name').css(highlightStyling)



//   .addclass
// SYNTAX: .addclass(className);
// $('.important').click(function () {
//     $(this).addClass('highlighted');
// });
//
// $('.important').dblclick(function () {
//     $(this).removeClass('highlighted');
// });


// .toggleClass()
$('.important').click(function () {
    $(this).toggleClass('highlighted');
});