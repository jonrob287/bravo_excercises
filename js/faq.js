'use strict';
$('#answers').click(function () {
    $('dd').toggleClass('invisible')
});
//
// 1. Create a button that, when clicked, will change the 'answers'
// background-color to a color of your choice.
$('#change-answers-background').click(function () {
    $('.answersBackground').toggleClass('changeColor')
})
// .answersBackground
// id="change-answers-background"

// 2. At the top of the page, add a div with two elements, an h1 that reads
// 'Would you like to sign up for more updates?' and a span that has an 'x' in
// it. When the 'x' is clicked, hide the entire div element.
$('#hide-div').click(function () {
    $('#sign-up-updates-container').hide();
})
// id="sign-up-updates-container"
// id="hide-div"

// 3. After the user has been on the page for 10 seconds, an h3 element should fade
// in that asks 'Register to our site?'
 $('#register-to-site').delay(10000).fadeIn(2000);
// id="register-to-site"
// $('h3').css('display: none')
// $('h3').fadeIn(10000)

    //Use a bootstrap alert for the dismissable message that shows on page load.
    //Use a bootstrap modal for the message that fades in after 8 seconds. You may
// find that you can use bootstrap's modal methods to achieve the fading effect.
