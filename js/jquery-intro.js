'use strict';

//The jQuery Object
// object used to find and creat HTML elements from the DOM

//Document Ready
// window.onload = function () {
//     alert('the page has finished loading!');
// }

// $(document).ready(function () {
//     alert('the page has finished loading!');
// });

// In jQuery, we use the dollar sign '$', to reference the jQuery object
// $ is an alias of jQuery.

// jQuery selectors
// ID selector         #id
// Class selector      .class
// Element selector    element ex h1
// Multiple selector   selector1, selector2,....
// ALL selectors           * selects all


// Syntax for jQuery selectors
// $('selector')

//
// .html - returns the html contents of selected element(s)
// Similar to 'innerHTML' property
//
// .css - allows us to change css properties for element(s)
// similar to the 'style' property

// ID SELECTOR
// SYNTAX for selection an element by an id:
// $('#id-name')

// var content = $('#codeBound').html();
// alert(content);

// alert($('#codeBound').html());


// CLASS SELECTORS
// SYNTAX for selecting an element with a class name:
// $('.class-name')

// $('.urgent').css({'background-color': 'red'});
//             OR
// $('.urgent').css('background-color', 'red');
// $('.urgent').css('text-decoration', 'underline');

// HOW TO ADD MULTIPLE ELEMENTS
// $('.urgent').css({'background-color': 'teal', 'font-size': '200%'});


// ELEMENT SELECTORS
// SYNTAX: $('element-name')

// $('p').css('font-size', '50px')

// MULTIPLE SELECTOR
// SYNTAX: $('selector1, selector2, ....')

// $('.urgent,p').css({'background-color': 'orange'});
// $('.urgent,p').css('background-color', 'orange');


// ALL SELECTOR
// SYNTAX: $(*)
// $('*').css('border', '4px dotted red')
// $('*').css({'border': '4px dotted red'});
