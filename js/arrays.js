(function () {   //IIFE GOES FIRST
    "use strict"

    /** TODO:
     *  Create a new EMPTY array named sports.**/
var sports = [];
    console.log(sports);
    /** Push into this array 5 sports**/
    sports.push('volleyball', 'tennis', 'football', 'baseball', 'basketball')
    console.log(sports);
    /** Sort the array.**/
    sports.sort();
    console.log(sports);
    /** Reverse the sort that you did on the array.**/
    sports.reverse();
    console.log(sports);
    /** Push 3 more sports into the array **/
    sports.push('water skiing', 'snow boarding', 'golf')
    console.log(sports);
    /** Locate your favorite sport in the array and Log the index. **/

    /** Remove the first sport in the array **/
    sports.shift();
    console.log(sports);
    /** Turn your array into a string  **/
    var stringOfSports = sports.join(', ')
    console.log(stringOfSports)

// ---------------------------Second Part of Exercise
    console.log('SECOND PART OF THE EXERCISE')


    /** Create a new array named bravos that holds the names of the people in your class**/
    var bravos = ['Adrian', 'Henry', 'Jonathan', 'MaryAnn', 'Sandra', 'Tameka', 'Eric']


    /** Log out each individual name from the array by accessing the index. **/
    var first = bravos[0] //Faith's Answer
    console.log(first)    //Faith's Answer
    console.log(bravos[0])
    console.log(bravos[1])
    console.log(bravos[2])
    console.log(bravos[3])
    console.log(bravos[4])
    console.log(bravos[5])
    console.log(bravos[6])


    /** Using a for loop, Log out every item in the bravos array **/
    for (var i = 0; i < bravos.length; i += 1) {
             console.log('The person at index' +' '+ i + ' ' + 'is' + ' ' + bravos[i]);
             console.log(bravos[i]) //Faiths answer
         }


    /** Refactor your code using a forEach Function **/
   bravos.forEach(element => console.log(element));
  //Faith's Answer
   bravos.forEach(function (entry) {
    console.log(entry)
   })


    /** Using a for loop. Remove all instance of 'CodeBound' in the array **/
    //Faith's Answer
    var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];
    for (var n = 0; n < arrays1.length; n++){
        if(arrays1[n] !== 'CodeBound'){
            console.log(arrays1[n]);
        }
    }


    /**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/
    var numbers = [3, '12', 55, 9, '0', 99];
    var sum = 0;
    numbers.forEach(function (num) {
        sum += parseInt(num) ;
    })
    console.log(sum)


    /**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/
   // Faith's Answer'
    var numbers1 = [7, 10, 22, 87, 30];
    numbers1.forEach(function(element,
                              index,
                              numbers1){
        numbers1[index] = element * 5;
    })
    console.log(numbers1)


})()