(function () {   //IIFE GOES FIRST
            "use strict"

    //Basic Syntax
//    []// an empty array
//  ['pink']// each individual value is considered an elements.
//  ['pink', 'purple', 'blue', 'black'] // 4 elements
//    [4,6,8,10,12]   array of numbers
//        [2,3,4,5,[2,4,6,8,]]    //Nested arrays
// var colors = ['black', 'green', 'blue', 'purple', 'yellow', 'red']
//
//     console.log(colors);
//             console.log(colors.length)
//

//------------------------------------accesing arrays
    //zero-indexed
//     var codebound   =['stephen', 'faith', 'karen', 'leslie', 'twyla'];
//     console.log(codebound[4])
//     console.log(codebound[3])
//     console.log(codebound[2])
//     console.log(codebound[1])
//     console.log(codebound[0])
//     console.log(codebound[5]) // undefined bc it is outside of the array
//
// console.log('There are ' + codebound.length + ' people on the Codebound Team.')
// console.log(codebound)


    //------------------------------------Iterating Array
    //traverse through elements....traveling thru the array
    // var codebound   =['stephen', 'faith', 'karen', 'leslie', 'twyla']

//looping through an array and console.log the values
//     for(var i = 0; i < codebound.length; i++){
//         console.log('Here is a person on the Codebound team: '+ codebound[i])
//         console.log(codebound[i]);
//         console.log('The person at the index ' + i + ' is ' + ' ' + codebound[i])
//     };

//------------------------------------------forEach Loop
//             BASIC SYNTAX
//             array.forEach(function (element,index,array) {
//             })
//                         keep naming conventions to plural nouns
//                  var numbers =[2,3,5,6,7,8,9,10]
//     var colors =['blue', 'black', 'purple', 'green']
//
//     colors.forEach(function (color) {
//             //do something
//         console.log(color)
// //     })
//
//     //------------------------------------Changing/Manipulating Arrays
//     var fruits = ['banana', 'apple', 'grape', 'strawberry']
//     //Adding to array
//     console.log(fruits)
//
//     // .push
//     fruits.push('watermelon');
//     console.log(fruits)
//     // .unshift add to the beginning of an array
//     fruits.unshift('cherry')
//     console.log(fruits)
//     // add multiple
//     fruits.push('mandarin', 'dragonfruit', 'starfruit');
//     console.log(fruits)
//
//
//     //-------------------------------------Remove/delete an element from an array
//     //     .pop  // removes the last element in the array
//     fruits.pop();
//     console.log(fruits)
//
//     //     .shift   // removes the first element in the array
//         fruits.shift();
//     console.log(fruits)
// var removedFruit = fruits.shift();
//     console.log('Here is the fruit I removed ' + removedFruit +'.');
//
//



    //----------------------------------Locate Array Elements
    // var fruits = ['banana', 'grape', 'apple', 'grape', 'strawberry']
    // .indexOf  //returns only the first occurrence of what you are looking for
    // var index = fruits.indexOf('grape')
    // console.log(index)
    // // .lastIndexOf // starts at the end of an array and returns index
    // index = fruits.lastIndexOf('grape')
    // console.log(index)

        //---------------------------------------Slicing
        //.slice copies a portion of the array
    //return a new array-doesnt modify original array
    // var fruits = ['banana', 'grape', 'apple', 'strawberry','mandarin', 'dragonfruit', 'starfruit' ]
    //     var slice = fruits.slice(2,5);
    //         console.log(slice);
    //         slice = fruits.slice(1)
    // console.log(slice)

//-----------------------------------------Reverse
    // .reverse
    //will reverse the original array AND return the reversed array.
    // var fruits = ['banana', 'grape', 'apple', 'strawberry','mandarin', 'dragonfruit', 'starfruit' ]
    // fruits.reverse();
    //         console.log(fruits)

    //-------------------------------------Sorting
    // .sort
    // will sort the original array AND return the sorted array.
    // var fruits = ['banana', 'grape', 'apple', 'strawberry','mandarin', 'dragonfruit', 'starfruit' ]
    // fruits.sort();
    //         console.log(fruits);



//----------------------------------------Splitting
    // .split
    // takes a string and turns it into an array.
    // var codebound = 'karen,faith,stephen'
    // console.log(codebound);
    //         var codeBoundArray = codebound.split(',');
    //         console.log(codeBoundArray)

    //---------------------------------Joining
    // .join
    // takes an array and converts it into a string with a delimiter of your choice
    var codeBoundArray = ['karen','faith','stephen']
    console.log(codeBoundArray)
    var newCodeBound = codeBoundArray.join(', ');
console.log(newCodeBound)











})()