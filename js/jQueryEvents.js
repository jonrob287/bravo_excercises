'use strict';
// alert('hello')
// How to use jQuery to attach event listeners to html elements

//MOUSE EVENTS
/*
.click() - event handler to the "click"

.dblclick() - event handler to the "double click"

.hover() - executed when the mouse pointer enters and leaves the element(s)
 */
//~~~~~~~~~~~~~~~~~~~MOUSE EVENTS IN JS
// var eventElement =document.getElementById('#my-id');
//
// eventElement.addEventListener('click', function (e) {
//     alert('My id was clicked!');
// })
//~~~~~~~~~~~~~~~~~~~MOUSE EVENTS IN JQUERY
//.click()
//SYNTAX : $('selector').click(handler);
//
// $('#bravo').click(function () {
// alert('H1 with the id of bravo was clicked!')
// })

//.dbl
// $('#bravo').dblclick(function () {
//     alert('H1 with the id of bravo was double clicked!')
// })

//.hover
//SYNTAX: $(selector).hover(handlerIn, handlerOut)
// $('#bravo').hover(
//     function () {
//         //handlerIn anonymous function
//         $(this).css('background-color', 'blue');
//     },
//     function () {
//         //handlerOut anonymous function
//         $(this).css('background-color', 'white');
//     }
// );

// $('#bravo').hover(function () {
//     alert('H1 with the id of bravo was hovered over!');
// });

//KEYBOARD EVENTS IN jQuery
/*
Different types of keyboard events:
    .keydown()
    .keypress()
    .keyup()
    .on()
    .off()
 */


// .keydown()   -   event handler to the "keydown" JavaScript event / trigger that event
// on an element
// SYNTAX: $(selector).keydown(handler)
// $('#my-form').keydown(
//     function () {
//         alert('Hey! You pushed down a key in the form!');
//     }
// );

// .keypress() - same as keydown, with one exception: Modifier keys (shift, control,esp.)
// will trigger will trigger .keydown() but not .keypress()
// SYNTAX: $(selector).keypress(handler)

//.keyup()
// SYNTAX: $(selector).keypress(handler)
// $('#my-form').keyup(
//     function () {
//         alert('Hey! You pushed down a key in the form and it is now released!');
//     }
// );

//.on() and .off()
$('.important').on('click',function () {
    console.log(s)
    alert("hello")
})

$('.important').off('click')







