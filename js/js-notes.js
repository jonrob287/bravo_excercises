"use strict" //helps us to create/write cleaner code

console.log("Hello from Console");  //method used to test our work in the console
console.log("Hi, From Karen")

// THIS IS A COMMENT
/*
THIS IS
A COMMENT
FOR MULTI LINES
 */

// DATA TYPES
// PRIMITIVE TYPES
// numbers, strings, boolean, undefined, null..... objects

// VARIABLES
// var, const

// NUMBERS - 100, -10, 0, 0.123
// STRINGS - "Stephen", "email@example.com", "qweruio"
// BOOLEAN - true, false
// if a user isTwentyOneOrOlder? true or false
// UNDEFINED -  unassigned variables have this value and type

// var person = undefined;

// NULL - a special keyword for nothing


// 99.9 number
// 'false' string
// false boolean
// '0' string
// 0 number


//      ****OPERATORS******
// built-in operators
Math.cos(9);
Math.pow(4,2);

// Basic arithmetic operators
//follow same rule of order
/*
+ (add);
- (substract)
* (multiply)
/ (divide)
% Modulus aka remainder
 */

(1 + 2) * 4 / 5;
3 * 4 / 5

//      ****LOGICAL OPERATORS******
//returns a boolean value when used with boolean values
//also used with non-boolean values but will return non-boolean value
/*
&&  (AND)
||  (OR)
!   (NOT)


&&  (AND)
-A user can edit a message if they are the admin && the author;
INPUT      OUTPUT
T && T     TRUE
T && F     FALSE
F && F     TRUE
F && T     FALSE


||  (OR)
-A user can edit a message if they are a registered user || the admin;
INPUT      OUTPUT
T || T     TRUE
T || F     TRUE
F || F     FALSE
F || T     TRUE


!   (NOT)(The opposite)
Redirect users if they are ! logged in;
INPUT      OUTPUT
!T         FALSE
!F         TRUE
!!!!!!!T   FALSE

EXAMPLE

false && false || true : true
true || false || true : true
true && false && false || true : true

//      ****COMPARISON OPERATORS******
//WILL RETURN A LOGICAL VALUE BASED ON WHETHER THE COMPARISON IS TRUE
//can be any data type
//4 comparison operators

== : check if the value is the same
=== : check if the value and the data type are the same
!= :check if the value is NOT the same
!== : check if the value and data type are NOT the same

EXAMPLES

4 == 4  : TRUE
4 === '4' : FALSE
4 != 5 : TRUE
4 == '4' : TRUE
4 == 5 : FALSE
4 !== 4 : FALSE
4 !== '4' : TRUE

7 !== 5 : TRUE
7 != '5' : TRUE
4 != '2' : TRUE
4 !== '2' : TRUE
4 !== FALSE : TRUE
4 !== 4 : FALSE


<,>,<=,>=
const age = 21

age < 18 : FALSE

 */
//CONCATENATION   FORMAT
const name = 'Stephen';
const age = 13;
console.log(name);
console.log(age);

console.log('Hello, my name is ' + name + ' and I am ' + age);

// TEMPLATE STRING    FORMAT
console.log(`Hello, my name is ${name} and I am ${age}`);



// GET THE LENGTH OF A STRING
console.log(name.length);

const s = ('Hello World');
console.log(s.length);

//GET THE STRING IN UPPERCASE
console.log(s.toUpperCase());

//GET THE STRING IN LOWERCASE
console.log(s.toLowerCase());

//INDEX
console.log(s.substring(0, 5));   //Hello
console.log(s.substring(6, 11)); //World

//USER INTERACTION
// alert()
// console.log('Message');
//alert('Hello there!');
//alert('Welcome to my website');

//confirm
var confirmed = confirm('Are you sure you want to continue?')
console.log(confirmed);

//prompt
var userInput = prompt('Enter your name: ');
console.log('The user enter: ' + userInput);
//FUNCTIONS

//CALL A FUNCTION
// call it by its name with ()
/*
nameOfTheFunction(); call the function
 */
function incrementOfOne(num1) {
return num1 + 1;
}
//call the function
console.log(incrementOfOne(5));
//NaN - Not a Number

//ANONYMOUS FUNCTIONS
var increment = function (num1) {
    return num1 +1
};
//CALL AN ANONYMOUS FUNCTIONS
 var two = increment(1);
 console.log(two);

function sumPart2(a, b) {
    var result = a + b;
    return result;
}
console.log(sumPart2(12,23))



 // A FUNCTION WITH A RETURN VALUE
function shout(message) {
    alert(message.toUpperCase()+'!!!');
}
var returnValue = shout('hey yall');

//FUNCTION WITH NO PARAMETER AND NO RETURN VALUE
// function greeting() {
//     alert('hey there!')
// }

function greeting(message){
    return message;
}
console.log(greeting('Hey Codebound!'))
