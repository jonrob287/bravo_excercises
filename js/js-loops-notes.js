'use strict';
//LOOPS
// While Loop, For Loop, Break and Continue

//WHILE LOOP
// Is a basic looping construct that will execute a block of code
// as long as a certain condition is true.

// SYNTAX:
/*
    while ('condition') {
        //run the code
    }
 */

var n = 0;
while (n <= 1) {
    console.log('#' + n);
    n++;
}
//DO WHILE
/*
the only difference from a while
loop is that the condition is evaluated
at the end of the loop. Instead of the beginning.
 */

//SYNTAX:
/*
do {
    //run the code
} while ('condition')
 */
var i = 10;
do {
    console.log('Do-While #' + i);
    i++;
}while(i <= 20)

//FOR LOOPS
/*
A for loop is a robust looping mechanism available in many programming
languages.
 */

//SYNTAX
/*
for ('initialization'; 'condition'; 'increment/decrement') {
        // run the code
  }
 */
for (var x = 0; x <= 10; x++){
    console.log('For Loop #' + x)
}

//BREAK AND CONTINUE
// -breaking out of a loop
// using a 'break' keyword allows us to exit the loop
var endAtNumber = 5;
for (var b = 1; b < 100; b++) {
    console.log('Loop count #'+ b );
    if (b === endAtNumber) {

        alert('We have reached the stopping point! Break!');

        break;

        console.log('Do you see this message?')

    }
}
// Continue the next iteration of a loop
// by using the 'continue' keyword
for (var n = 1; n<=100; n++){
    if (n %2 !== 0){
        continue;

    }
    console.log('Even number : ' + n)
}



