(function () {   //IIFE GOES FIRST
    "use strict";
    // 1. create a for loop function that logs every 'Pop' album
    // const albums = [
    //     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    //     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    //     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    //     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    //     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    //     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    //     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    //     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    //     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    //     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    //     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    //     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    //     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    //     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    //     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    //     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    //     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    //     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    //     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    //     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
    // ]
    // for (var i = 0; i < albums.length; i += 1) {
    //     if (albums[i].genre === 'Pop'){
    //         console.log('Artist: ' + albums[i].artist + ' \nAlbum Title: ' + albums[i].title + ' \nAlbum Release Year: '+ albums[i].released + ' \nAlbum Genre: '+ albums[i].genre)
    //     }
    // }


    // 2. create a for each function that logs every 'Rock' album
   // const albums = [
   //      {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
   //      {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
   //      {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
   //      {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
   //      {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
   //      {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
   //      {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
   //      {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
   //      {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
   //      {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
   //      {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
   //      {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
   //      {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
   //      {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
   //      {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
   //      {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
   //      {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
   //      {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
   //      {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
   //      {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
   //  ]
    //          Faith's Answer

    // albums.forEach(function(album,x){
    //
    //     if(albums[x].genre === 'Rock'){
    //     }
    //     console.log(albums[x].title)
    // })

    // albums.forEach(function(rock){
    //
    //    if(rock.genre === 'Rock'){
    //        console.log(rock)
    //    }
    // })

    // 3. create a for each function that logs every album released before 2000
    // const albums = [
    //     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    //     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    //     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    //     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    //     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    //     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    //     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    //     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    //     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    //     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    //     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    //     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    //     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    //     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    //     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    //     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    //     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    //     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    //     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    //     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
    // ]
    //              Faith's Solution
    // albums.forEach(function(album,i){
    //
    //     if(albums[i].released < 2000 ){
    //         console.log(albums[i].released)
    //     }
    // })
    //          Stephen's Solution
    // albums.forEach(function(album){
    //
    //     if(album.released < 2000){
    //         console.log(album.released + ' ' + album.title);
    //     }
    // })
    //          My Solution
    // albums.forEach(function(before){
    //
    //     if(before.released < 2000){
    //         console.log(before)
    //     }
    // })

    // 4. create a for loop function that logs every album between 1990 - 2020
    // const albums = [
    //     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    //     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    //     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    //     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    //     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    //     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    //     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    //     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    //     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    //     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    //     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    //     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    //     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    //     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    //     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    //     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    //     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    //     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    //     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    //     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
    // ]
    // for (var i = 0; i < albums.length; i += 1) {
    //     if (albums[i].released >= 1990 && albums[i].released <= 2020){
    //         console.log('Artist: ' + albums[i].artist + ' \nAlbum Title: ' + albums[i].title + ' \nAlbum Release Year: '+ albums[i].released + ' \nAlbum Genre: '+ albums[i].genre)
    //     }
    // }

    // 5. for loop function that logs every Michael Jackson album
    // const albums = [
    //     {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
    //     {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
    //     {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
    //     {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
    //     {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
    //     {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
    //     {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
    //     {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
    //     {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
    //     {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
    //     {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
    //     {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
    //     {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
    //     {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
    //     {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
    //     {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
    //     {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
    //     {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
    //     {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
    //     {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
    // ]
    //
    // for (var i = 0; i < albums.length; i += 1) {
    //     if (albums[i].artist === 'Michael Jackson'){
    //         console.log('Artist: ' + albums[i].artist + ' \nAlbum Title: ' + albums[i].title + ' \nAlbum Release Year: '+ albums[i].released + ' \nAlbum Genre: '+ albums[i].genre)
    //     }
    // }

    // 6. create a function name 'addAlbum' that accepts the same parameters from
    // 'albums' and add it to the array
    const albums = [
        {artist: 'Michael Jackson', title: 'Thrillers', released: 1982, genre: 'Pop'},
        {artist: 'AC/DC', title: 'Back in Black', released: 1980, genre: 'Rock'},
        {artist: 'Pink Floyd', title: 'The Dark Side of the Moon', released: 1973, genre: 'Rock'},
        {artist: 'Bee Gees', title: 'Saturday Night Fever', released: 1977, genre: 'Disco'},
        {artist: 'Fleetwood Mac', title: 'Rumours', released: 1977, genre: 'Rock'},
        {artist: 'Shania Twain', title: 'Come On Over', released: 1997, genre: 'Country'},
        {artist: 'Michael Jackson', title: 'Bad', released: 1987, genre: 'Pop'},
        {artist: 'Led Zeppelin', title: 'Led Zeppelin IV', released: 1971, genre: 'Rock'},
        {artist: 'The Beatles', title: '1', released: 2000, genre: 'Rock'},
        {artist: 'Whitney Houston', title: 'Whitney', released: 1987, genre: 'Pop'},
        {artist: 'Def Leppard', title: 'Hysteria', released: 1987, genre: 'Rock'},
        {artist: 'Tupac', title: 'All Eyez on Me', released: 1996, genre: 'Rap'},
        {artist: 'Eminem', title: 'The Marshall Mathers LP', released: 2000, genre: 'Rap'},
        {artist: 'Green Day', title: 'Dookie', released: 1994, genre: 'Rock'},
        {artist: 'Michael Jackson', title: 'Dangerous', released: 1991, genre: 'Pop'},
        {artist: 'The Notorious B.I.G', title: 'Ready to Die', released: 1994, genre: 'Rap'},
        {artist: 'Adele', title: '21', released: 2011, genre: 'Pop'},
        {artist: 'Metallica', title: 'Load', released: 1996, genre: 'Rock'},
        {artist: 'Prince', title: '1999', released: 1982, genre: 'Pop'},
        {artist: 'Lady Gaga', title: 'Born This Way', released: 2011, genre: 'Pop'}
        ]
function addAlbum(artist, title, released, genre) {
        albums.push(
            {
                'artist': artist,
                'title': title,
                'released': released,
                'genre': genre,
            }
        )
}
addAlbum('Judas Priest', 'Hell Bent', '1982', 'Rock')
    console.log(albums);



})()