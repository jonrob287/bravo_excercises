'use strict'
/** We can create a promise obect like so.: */

// const myPromise = new Promise((resolve, reject) => {
//     if (Math.random()>0.5){
//         resolve();
//     } else{
//         reject();
//     }
// });
// myPromise.then(()=> console.log('resolved'));
// myPromise.then(()=> console.log('rejected'));


/** Discovering pending*/
// const myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if(Math.random() > 0.5){
//             resolve();
//         }else{
//             reject();
//         }
//     }, 1500);
// });
// console.log(myPromise);// A pending promise
//
// // myPromise.then(()=> console.log('resolved'));
// myPromise.then(()=> console.log('rejected'));

/**
 * Write a promise that stimulates an api request
 */

// function makeRequest(){
//     return new Promise((resolve, reject)=>{
//         setTimeout(() => {
//             if (Math.random()>0.1){
//                 resolve('Here is your data: ')
//             }else{
//                 reject('Network connection error.');
//             }
//         },1500);
//     });
// }
// const request = makeRequest();
// console.log(request);
// request.then(message => console.log('Promise resolved', message));
//
// request.catch(message => console.log('Message rejected', message));
/** NEW */
// const data = {username:'example'};
//
// fetch('surprise.html', {
//     method: 'Post',
//     headers: {
//         'Content-Type':'application/json',
//     },
//     body: JSON.stringify(data),
// })
//     .then(response => response.json())
//     .then(data => {
//         console.log('success ', data)
//     })
//     .catch((error)=> {
//         console.log('Error fetch gone bad', error);
//     })
//

//
// const isMomHappy = true;
//
// //Promise
// const willGetNewPhone = new Promise(((resolve, reject) => {
//     if(isMomHappy){
//         const phone = {
//             brand: 'Samsung',
//             color: 'black',
//         };
//         resolve(phone);
//     }else{
//         const reason = new Error('Mom is NOT happy.')
//         reject(reason);
//     }
// }))
//
// //2nd Promise
//
// const showOff = function (phone) {
//     const message = "Hey friend I have a new " + phone.color + ' ' + phone.brand + ' phone.';
//     return Promise.resolve(message);
// }
// // call our promise
// const askMom = function () {
//     willGetNewPhone
//         .then(showOff)
//         .then(fulfilled => console.log(fulfilled))
//         .catch(error=> console.log(error.message))
// }
// askMom();
//
//

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
// EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
// EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));

//
// const myPromise = new Promise((resolve, reject) => {
//     if(Math.random()>0.5){
//         resolve();
//     } else{
//         reject();
//     }
// });
// myPromise.then(() => console.log('resolved'));
// myPromise.catch(() =>  console.log('rejected'));

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MY SOLUTION WITH OUT ACCEPTING A PARAMETER of 4 OR 8 SECONDS
// function wait() {
//     return new Promise((resolve, reject)=>{
//         setTimeout(() => {
//             if (Math.random()>0.1){
//                 resolve('You will see this after 4 seconds.')
//             }else{
//                 reject('Network connection error.');
//             }
//         },4000);
//     });
// }
// const request = wait();
// console.log(request);
// request.then(message => console.log(message));
// request.catch(message => console.log('Message rejected', message));
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// function wait8() {
//     return new Promise((resolve, reject)=>{
//         setTimeout(() => {
//             if (Math.random()>0.1){
//                 resolve('You will see this after 8 seconds.')
//             }else{
//                 reject('Network connection error.');
//             }
//         },8000);
//     });
// }
// const request8 = wait8();
// console.log(request);
// request8.then(message => console.log(message));
// request8.catch(message => console.log('Message rejected', message));

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// STEPHEN'S ANSWER WITH ACCEPTING PARAMETER FOR SECONDS
// const wait = (delay) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(`You will see this after ${delay / 1000 } seconds`);
//         }, delay);
//     });
// };
// wait(4000).then((message) => console.log(message));
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Write a function testNum that takes a number as an argument and returns a Promise that tests if the value is less than or greater than the value 10.
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~MY SOLUTION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// function testNum(a) {
//     return new Promise((resolve, reject) => {
//         if (a >= 10) {
//             resolve('The value is greater or equal to 10.')
//         } else {
//             reject('The value is less than 10')
//         }
//     });
// }
// const numberTest = testNum(11);
// // console.log(request);
// numberTest.then(message => console.log(message));
// numberTest.catch(message => console.log(message));
//
// const numberTestLower = testNum(4);
// // console.log(request);
// numberTestLower.then(message => console.log(message));
// numberTestLower.catch(message => console.log(message));

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~FAITH'S SOLUTION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// const compareToTen = (num)=> {
//     const p = new Promise(((resolve, reject) => {
//     if (num > 10){
//         resolve (num + ' is greater than 10 success.')
//     } else{
//         reject(num + ' is less than 10 error!')
//     }
// }))
//         return p;
// }
// compareToTen(0)
//     .then(result => console.log(result))

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~QUESTION 3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//Write two functions that use Promises that you can chain! The first function, makeAllCaps(), will take in an array of
// words and capitalize them, and then the second function, sortWords(), will sort the words in alphabetical order.
// If the array contains anything but strings, it should throw an error.

const monuments = ['Colosseum','St. Peter\'s Basilica','Vatican Museums','Duomo di Monreale','Florence Cathedral','Ducal Palace','St Mark\'s Basilica'];

const complicatedMonuments = ['Colosseum', 76, true]

const makeAllCaps = (array) => {
            return new Promise((resolve, reject) => {
                var capsArray = array.map( word => {
                    if(typeof word === 'string'){
                        return word.toUpperCase()
                    }else{
                        reject('Error not items in the array are strings.')
                    }
                })
                resolve(capsArray)
            })
}

const sortWords = (array) => {
            return new Promise((resolve, reject) => {
                if (array){
                    array.forEach((el)=>{
                        if(typeof el !== 'string'){
                            reject('Error not items in the array are strings.')
                        }
                    })
                    resolve(array.sort())
                }else{
                    reject('Something went wrong with sorting the words in the array.')
                }
            })
}

makeAllCaps(monuments)
    .then(sortWords)
    .then((result)=> console.log(result))
    .catch(error => console.log(error))


makeAllCaps(complicatedMonuments)
    .then(sortWords)
    .then((result)=> console.log(result))
    .catch(error => console.log(error))

