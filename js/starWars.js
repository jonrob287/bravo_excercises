'use strict'
$(document).ready(function(){
    $.ajax({
        url: 'data/starWars.json',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var name = response[i].name;
                var height = response[i].height;
                var mass = response[i].mass;
                var birth_year = response[i].birth_year;
                var gender = response[i].gender;
                var tr_str = "<tr>" +
                    "<td align='center'>" + name + "</td>" +
                    "<td align='center'>" + height + "</td>" +
                    "<td align='center'>" + mass + "</td>" +
                    "<td align='center'>" + birth_year + "</td>" +
                    "<td align='center'>" + gender + "</td>" +
                    "</tr>";
                $("#characters tbody").append(tr_str);
            }
        }
    });
});
//
//
//
//
// $.ajax('data/starWars.json',{
//     type: 'Get',
// }).done(function (data) {
//     $.each(data, function (index, value) {
//     console.log(value)
//     var name = value.name;
//     var height = value.height;
//     var mass = value.mass;
//     var birth_year = value.birth_year;
//     var gender = value.gender;
//     var char = '<tr>' +
//         '<td>' + name + '</td>' +
//         '<td>' + height + '</td>' +
//         '<td>' + mass + '</td>' +
//         '<td>' + birth_year + '</td>' +
//         '<td>' + gender + '</td>' + '</tr>'
//         $('#insertCharacters').append(char);
// })
// })
