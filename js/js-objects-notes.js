(function () {   //IIFE GOES FIRST
    "use strict";
    //OBJECTS
    // Object is a grouping of data and functionality.
    // Data items inside of an object = PROPERTIES
    // Function inside of an object = METHODS
    //
    // CUSTOM OBJECTS
    // Prototype allows existing objects to be used as templates to create new objects.
    // Object keyword - the starting point to make custom objects.

    // var car = new Object();
    // console.log(typeof car);
    //object

    //The use of 'new Object()' calls the Object CONSTRUCTOR to build a new INSTANCE of Object.

    // OBJECT LITERAL NOTATION
    // - curly braces {}
    // var car = {};
    // alert(typeof car);
    // object

//SETTING UP PROPERTIES ON A CUSTOM OBJECT
//     var car = {};
//
//     //dot notation
//     car.make = 'Toyota';
//
//     //array notation
//     car['model'] = '4Runner';
//
//     console.log(car);

    //Another way/ most common way to assign properties
    //
    // var car = {
    //     make: 'Toyota',
    //     model: '4Runner',
    //     year: '2019',
    //     color: 'Red',
    //     numberOfWheels: 4
    // }
 // console.log(car)

    // DON'T DO THIS
//car['numberOfDoors'] = 4
    //INSTEAD
//    car.numberOfDoors = 5
    // console.log(car)


//NESTED VALUES

var cars = [
    {
        make: 'Toyota',
        model: '4Runner',
        year: '2019',
        color: 'Red',
        numberOfWheels: 4,
        features: ['Automatic Windows', 'Bluetooth Connection'],
        alarm: function () {
            alert('Sound the alarm!')

        }
    },
    {
        make: 'Honda',
        model: 'Civic',
        year: '2020',
        color: 'Green',
        numberOfWheels: 4,
        features: ['4 cylinder', 'AM/FM Radio'],
        Owner:{
            name: 'John Doe',
            age: 40
        },
        alarm: function () {
            alert('No alarm sorry')

        }
    }
];


console.log(cars[0].features[1]);
console.log(cars[1].Owner);
cars.forEach(function (car) {
    car.features.forEach(function (feature) {
        console.log(feature)
    })
})


console.log(cars[1].alarm());


// var car = {};
//
// car.make = 'Ford';
// car.model = 'Mustang';
//
// // create a method on the car object
//     car.describeCar = function () {
//     console.log('Car make and model is: ' + this.make + ' ' + this.model);
//     }
//     // calling the object's method
//     car.describeCar();
//
// var car = {
//     make: 'Ford',
//     model: 'Mustang',
//     describeCar: function () {
//         console.log('Car make and model is: ' + this.make + ' ' + this.model);
//     }
// }
// car.describeCar();













})()