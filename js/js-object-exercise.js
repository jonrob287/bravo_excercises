(function () {   //IIFE GOES FIRST
    "use strict";
/**
* TODO: done
* Create an object with firstName and lastName properties that are strings
* with your first and last name. Store this object in a variable named
* `person`.
*
* Example:
*  > console.log(person.firstName) // "Tim"
*  > console.log(person.lastName) // "Duncan"
**/
// var person = {
//     firstName: 'Jonathan',
//     lastName: 'Robles',
//     }
//     console.log(person.firstName);
//     console.log(person.lastName);

/**
 * TODO: done
 * Add a sayHello method to the person object that returns a greeting using
 * the firstName and lastName properties.
 * console.log the returned message to check your work
 *
 * Example
 * > console.log(person.sayHello()) // "Hello from Tim Duncan!"
 **/
// var person = {
//     firstName: 'Jonathan',
//     lastName: 'Robles',
//     sayHello: function () {
//         console.log(`Hello from ${this.firstName} ${this.lastName}!`)
//     }
// }
//     person.sayHello();


    /** TODO: done
     * HEB has an offer for the shoppers that buy products amounting to
     * more than $200.
     *
     * If a shopper spends more than $200,
     * they get a 12% discount.
     *
     * Write a JS program, using conditionals,
     * that logs to the browser,
     * how much Karen,Pibo and Juan need to pay.
     * We know that Pibo bought $180, Karen $250 and Juan $320.
     *
     * Your program will have to
     * display a line with the name of the person,
     * the amount before the
     * discount,
     * the discount, if any, and the amount after the discount.
     *
     * Use a foreach loop to iterate through the array,
     * and console.log the relevant messages for each person
     *
     * Use the following array of objects
     *
            var shoppers = [
                {name: 'Pibo', amount: 180},
                {name: 'Karen', amount: 250},
                {name: 'Juan', amount: 320},
            ];
**/
    // var shoppers = [
    //     {
    //         name: 'Pibo',
    //         amount: 180,
    //     },
    //     {
    //         name: 'Karen',
    //         amount: 250,
    //     },
    //     {
    //         name: 'Juan',
    //         amount: 320,
    //     }
    // ];
    // var discount = .12;
    // shoppers.forEach(function (shopper) {
    //     var output = `${shopper.name} bought $${shopper.amount} worth of groceries.`;
    //
    //     if (shopper.amount >= 200 ){
    //
    //         output += '\nDiscount: ' + discount + '\nTotal: $ ' + (shopper.amount) * (1 - discount);
    //
    //     }
    //     console.log(output)
    // })

//  WHAT I GOT

    // if(shoppers.amount > 200){
    //    var discount = shoppers.amount * .12
    //     console.log(discount)
    // }


/** TODO: done
 * Create an array of objects that represent books and store it in a
 * variable named `books`. Each object should have a title and an author
 * property. The author property should be an object with properties
 * `firstName` and `lastName`. Be creative and add at least 5 books to the
 * array
 *
 *
 * Example:
 * > console.log(books[0].title) // "IT"
 * > console.log(books[0].author.firstName) // "Stephen"
 * > console.log(books[0].author.lastName) // "King"
 **/
//
// var books = [
//     {
//         title: 'Pride and Prejudice',
//         author: {
//             firstName: 'Jane',
//             lastName: 'Austen',
//         }
//     },
//     {
//         title: 'To Kill a Mockingbird',
//         author: {
//             firstName: 'Harper',
//             lastName: 'Lee',
//         }
//     },
//     {
//         title: 'The Da Vinci Code',
//         author: {
//             firstName: 'Dan',
//             lastName: 'Brown',
//         }
//     },
//     {
//         title: 'The Hunger Games',
//         author: {
//             firstName: 'Suzanne',
//             lastName: 'Collins',
//         }
//     },
//     {
//         title: 'Lord of the Flies',
//         author: {
//             firstName: 'William',
//             lastName: 'Golding',
//         }
//     },
// ]
//     console.log(books[2].title)
//     console.log(books[2].author.firstName)
//     console.log(books[2].author.lastName)

/**
 * TODO: done
 * Loop through the books array and output the following information about
 * each book:
 * - the book number (use the index of the book in the array)
 * - the book title
 * - author's full name (first name + last name)
 **/

/**
 * Example Console Output:
 *
 *      Book # 1
 *      Title: IT
 *      Author: Stephen King
 *      ---
 *      Book # 2
 *      Title: The Handmaid's Tale
 *      Author: Margaret Atwood
 *      ---
 *      Book # 3
 *      Title: Fire and Blood
 *      Author: George R.R. Martin
 *      ---
 *      ...
 **/

var books = [
    {
        title: 'Pride and Prejudice',
        author: {
            firstName: 'Jane',
            lastName: 'Austen',
        }
    },
    {
        title: 'To Kill a Mockingbird',
        author: {
            firstName: 'Harper',
            lastName: 'Lee',
        }
    },
    {
        title: 'The Da Vinci Code',
        author: {
            firstName: 'Dan',
            lastName: 'Brown',
        }
    },
    {
        title: 'The Hunger Games',
        author: {
            firstName: 'Suzanne',
            lastName: 'Collins',
        }
    },
    {
        title: 'Lord of the Flies',
        author: {
            firstName: 'William',
            lastName: 'Golding',
        }
    },
]
    for (var i = 0; i < books.length; i += 1) {
        console.log(
            'Book # ' + i +
            '\nTitle: ' + books[i].title +
            '\nAuthor: ' + books[i].author.firstName + ' ' + books[i].author.lastName
        )
        // console.log('Book # ' + ' ' + i );
        // console.log('Title: ' + books[i].title);
        // console.log('Author: ' + books[i].author.firstName + ' ' + books[i].author.lastName);
    }


/**
 * Bonus:
 * - Create a function named `createBook` that accepts a title and author
 *   name and returns a book object with the properties described
 *   previously.
 *   Refactor your code that creates the books array to instead
 *   use your function.
 * - Create a function named `showBookInfo` that accepts a book object and
 *   outputs the information described above. Refactor your loop to use your
 *   `showBookInfo` function.
 *   **/






 })()