'use strict';


//MAP, FILTER, AND REDUCE
// -all functions that operates on collection (arrays)
// -all will NOT modify but will return a new copy of the array

// .map = transform each element in the collection
// .filter = filters our values
// .reduce = reduces a collection to a single value

// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// var otherNumbers = [11, 12, 13, 14, 15, 16, 17, 18, 19, 10]
// var evens = [];
// for(var i = 0; i<numbers.length; i++){
//     if (numbers[i] % 2 === 0){
//         evens.push(numbers[i]);
//     }
// }
// console.log(evens)
//
// var odds =[]
// for(var i = 0; i<numbers.length; i++){
//     if (numbers[i] % 2 !== 0){
//         odds.push(numbers[i]);
//     }
// }
// console.log(odds)

// var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
//
// var evens = numbers.filter(function (n) {
//     return n % 2 === 0;
// });
// console.log(evens)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~ES6
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const evens = numbers.filter(n => n % 2 === 0);
console.log(evens)

// var odds = numbers.filter(function (n) {
//     return n % 2 !== 0;
// });
// console.log(odds)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~ES6
const odds = numbers.filter(n => n % 2 !== 0);
console.log(odds)


// MAP
// var increment = numbers.map(function (num) {
//     return num + 1;
// })
// console.log(increment)

//~~~~~~~~~~~~~~~~~~~~~~~~~~ES6
const increment = numbers.map(num => num +1)
console.log(increment)

//REDUCE
// .reduce =used to ' reduce' a collection to a single value.
const nums = [1, 2, 3, 4, 5];
const sum = nums.reduce((accumulation, currentNumber)=>{
   return accumulation + currentNumber
}, 0);
console.log(sum);

const officeSales = [
    {
        name: 'Jim Halpert',
        sales: 500
    },
    {
        name: 'Dwight Schrute',
        sales: 750
    },
    {
        name: 'Ryan Howard',
        sales: 150
    }
];

const totalSales = officeSales.reduce((totals, person)=>{
    return totals + person.sales;
}, 0);
console.log(totalSales);

const salesNeededToMakeQuota = officeSales.reduce((quota, person)=>{
    return quota - person.sales;
}, 20000);
console.log(salesNeededToMakeQuota);












