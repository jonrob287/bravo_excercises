'use strict';
//


// While Loops
var n = 2;
while (n < 65538) {
    console.log(n);
    n *= 2;
}
//For Loop
function showMultiplicationTable(x) {
    var a = x;
    var b;
    for (var i = 1; i <= 10; i ++) {
        b = a * i;
        console.log(+ a + ' x ' + i + ' =', + b + ' ' );
    }
}
console.log(showMultiplicationTable(8))


//FOR LOOPS
for (var x = 100; x > 0; x-=5){
    console.log(x)
}


// prompt user for odd number 1 - 50
var userInput = parseInt(prompt('Enter an odd number between 1 and 50.'));
if (userInput < 0 || userInput > 50){
    alert('Invalid Input! Must enter a number between 1 and 50')
}else {
// use loop and break statement to continue prompting user if invalid input
    for (var b = 1; b <= 50; b++) {
        if (b === userInput) {
            console.log('Yikes! We are skipping number: ' + b);
            continue;

        }
        if (b % 2 !== 0) {
            console.log(' Odd number: ' + b);
        }
    }
}

// FOR LOOP



for (var i = 0; i <= 20; i++) {
    if (i % 2 !== 0){
        console.log(i +' is odd.')
    }else {
        console.log(i +' is even.')
    }
}


