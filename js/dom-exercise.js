(function () {   //IIFE GOES FIRST
    "use strict";


    /**1. When the button with the id of `change-bg-color` is clicked the background of
     the page should turn blue.*/
    /**Completed*/
var changeBgColorBtn = document.getElementById('change-bg-color');
    console.log(changeBgColorBtn);
    changeBgColorBtn.addEventListener('click',function () {
        document.body.style.background = 'blue';
    });



    /**2. When the button with an id of `append-to-ul` is clicked, append an li with
     the content of `text` to the ul with the id of `append-to-me`.*/
    var appendToUl = document.getElementById('append-to-ul');
    function addList() {
        var ul = document.getElementById('append-to-me')
        var li = document.createElement("li");
       // var children = ul.children.length + 1
        //li.setAttribute('id', 'element'+ children)
        li.appendChild(document.createTextNode("Text"));
        ul.appendChild(li)
    }
appendToUl.addEventListener('click',addList)


    /**3. Two seconds after the page loads, the heading with the id of `message` should
     change it's text to "Goodbye, World!". Completed*/
var goodbye = document.getElementById('message');
    function changeHeading() {
        setInterval(function () {
            goodbye.innerHTML = 'Goodbye, World!'
        },2000);
    }
    changeHeading();
    // clearInterval();

    /**4. When a list item inside of the ul with the id of `hl-toggle` is first
     clicked, the background of the li that was clicked should change to
     yellow. When a list item that has a yellow background is clicked, the
     background should change back to the original background.*/
    //   Faith's Answer             NOT WORKING!!!
    Array.from(document.getElementById('hl-toggle').children).forEach(function (elem) {
            elem.addEventListener('click',function (e) {
                if(e.target.style.backgroundColor === 'yellow'){
                    return e.target.style.backgroundColor = 'white';
                }else {
                    return e.target.style.backgroundColor = 'yellow'
                }
            })
    })




    //  MY Answer
// var liBgToggle = document.getElementById('hl-toggle');
// function triggerToggle() {
// if (liBgToggle.style.background === 'white'){
//     liBgToggle.style.background ='yellow'
// }else {liBgToggle.style.background = 'white'}
// }
// liBgToggle.addEventListener('click',triggerToggle)


    /**5. When the button with the ID of `upcase-name` is clicked, the element with the
     ID of `output` should display the text "Your name uppercased is: " + the
     value of the `input` element with the ID of `input` transformed to uppercase.*/
                //Faith's Answer  NOT WORKING!!!!!!!!!

    // var button1 = document.getElementById('upcase-name')
    //
    // button1.addEventListener('click',function () {
    //
    //     var mainParagraph = document.getElementById('output')
    //
    //     var entry = document.getElementById('input')
    //
    //     UpperCasedEntry = entry.value.toUpperCase()
    //
    //     mainParagraph.innerHTML = 'Your name uppercased is : ' + upperCasedEntry;
    //
    // })


    // /// My Answer           WORKS!!!
    var outputText = document.getElementById('output');
    var inputText = document.getElementById('input');
     var upperName = inputText.value.toUpperCase();
    var upperCaseBtn = document.getElementById('upcase-name');
    upperCaseBtn.addEventListener('click',function convertCase(text) {
        var strUCase = input.value.toUpperCase();
        outputText.innerHTML = `Your name uppercased is: ${strUCase}`;
        // outputText.innerText = `Your name uppercased is: ${upperName}`;
    })

    /**6. Whenever a list item inside of the ul with the id of `font-grow` is _double_
     clicked, the font size of the list item that was clicked should double.*/

    var liFontGrow = document.getElementById('font-grow')
function makeFontDouble() {
        liFontGrow.style.fontSize = '2em'
}
liFontGrow.addEventListener("dblclick", makeFontDouble)

})()