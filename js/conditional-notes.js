"use strict"
// Conditional Statements
//if statements
//will will only if it meets certain conditions. That condition will return true or false.

//Basic Syntax
/*
if (condition) {
 */
    //execute this code if the condition is met

//EXAMPLE
/*
var numberOfLives = 0;
if (numberOfLives === 0) {
    console.log("Game Over")
    alert("Game Over")
}
*/


//   if/else statements
//      will will only once if it meets certain conditions. That condition will return true or false
//      else statement- will only run once when the condition is false

/// Basic Syntax
//
// if (condition){
//     //code here runs once the condition is met
// }   else{
//     // run this code only if the condition is a false statement
// }
//Example
    var b = 1;
if (b === 10) {
    console.log('b is 10');
} else {
    console.log('b is NOT 10');
}


// Switch Statements
// less duplicated and increases readability in code

//Basic Syntax
// switch (condition) {
//     case '': //code that gets executed
//     break;
//     case'':
//     break;
//     case '':
//     break;
//     default   //equivalent to else statement
//     break;
// }
var color = 'pink';
switch (color) {
    case 'blue':
        console.log('You chose blue');
        break;
    case'red':
        console.log('You chose red');
        break;
    case 'pink':
        console.log('Pink is the best color in the World!')
    default:
        console.log('You chose the wrong color!')
}

// Ternary Operators
// Shorthand way of creating if/else statements
// only used when there are two choices

// Basic Syntax
//          (if this condition is true) ? run this code : otherwise  run this code instead
/// EXAMPLE

var numberOfLives = 2;
(numberOfLives === 0) ? console.log("Game Over") : console.log('I\'m still alive')










