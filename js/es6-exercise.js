'use strict'
/*
 * Complete the TODO items below
 */
const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'php', 'java']
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'angular', 'spring boot']
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['java', 'aws', 'php']
    },
    {
        name: 'faith',
        email: 'faith@codebound.com',
        languages: ['javascript', 'java', 'sql']
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql']
    },
];
// TODO: replace the `var` keyword with `const`, then try to reassign a variable declared as `const`            */ DONE
// TODO: fill in your name and email and add some programming languages you know to the languages array         */ DONE

const name = 'Jonathan';
const email = 'jonathan@codebound.com';
const languages = ['html', 'css', 'javascript', 'AJAX'];


// TODO: rewrite the object literal using object property shorthand  */ DONE
// developers.push({
//     name: name,
//     email: email,
//     languages: languages
// });
// console.log(developers);
    developers.push({
        name,
        email,
        languages
    });
console.log(developers);

// // TODO: replace `var` with `let` in the following variable declarations  */ DONE
let emails = [];
let names = [];
//
//
// // TODO: rewrite the following using arrow functions
// developers.forEach(function(developer) {
//     return emails.push(developer.email);
// });
// // console.log(emails);
//arrow function
developers.forEach(developer => emails.push(developers.email));
console.log(emails)


// developers.forEach(function(developer) {
//     return names.push(developer.name);
// });
// console.log(names);
//arrow function
developers.forEach(developer => names.push(developer.name));
console.log(names);


//arrow function
// users.forEach(user => names.push(user.name));
// console.log(names);


// developers.forEach(function (developer) {
//     return languages.push(developer.languages);
// });
// arrow function
developers.forEach(developer => languages.push(developer.languages));
console.log(languages)




//
// // TODO: replace `var` with `const` in the following declaration  */ DONE
const developerTeam = [];
developers.forEach(function(developer) {


    // TODO: rewrite the code below to use object destructuring assignment
    //       note that you can also use destructuring assignment in the function
    //       parameter definition
    // const name = developer.name;
    // const email = developer.email;
    // const languages = developer.languages;

    const {name, email, languages} = developer;

    // TODO: rewrite the assignment below to use template strings
//     developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
// });
// console.log(developers);

    developerTeam.push(`${name}'s email is  ${email} ${name} knows ${languages.join(', ')}`);
});
console.log(developers);



// // TODO: Use `const` for the following variable */ DONE
    const list = '<ul>';
//
//
// // // TODO: rewrite the following loop to use a for..of loop
    developers.forEach(function (developer) {
//

        for (const developer of developers) {
            list += '<li>' + ${developer} +'</li>'
        };
    });
// //     // TODO: rewrite the assignment below to use template strings
//         list += '<li>' + developer + '</li>';


    //rewrite


    list += '</ul>';

    document.write(list);














































