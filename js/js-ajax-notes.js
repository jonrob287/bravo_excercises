'use strict'
// STANDARD JSON FORMAT
// {
//     "name1": "value1",
//     "name2": "value2",
//
// }

// JSON can be any data type
//     {
//         "string": "stringValue",
//         "num": 656,
//         "object": {
//             "prop": "value"
//         },
//         "array": [
//             {
//             "prop": "value", "prop2": "value2"
//             }
//             ],
//         "boolean": true/false,
//         "null": null
//     }
