"use strict"
    // /**
    //  * TODO:
    //  * Write a function named isOdd(x).
    //  * This function should return a boolean value.
    //  **/

    function isOdd(x) {
        return x % 2 !== 0;
    }
    console.log(isOdd(3))

    // /**
    //  * TODO:
    //  * Write a function named isEven(x).
    //  * This function should return a boolean value.
    //  **/
    // /**
function isEven(x) {
return x % 2 === 0;
}
console.log(isEven(4))

    //  * TODO:
    //  * Write a function named isSame(input).
    //  * This will return the input as the same in value and datatype return true.
    //  **/
    // /**
function isSame(input) {
return input;
}
console.log(isSame('yes'))

//  * TODO:
    //  * Write a function named isSeven(x).
    //  * This function should return a boolean value.
    //  **/
    // /**
function isSeven(x) {
    return x === 7;
}
console.log(isSeven(7))

    //  * TODO:
    //  * WriTe a function named addTwo(x).
    //  * This should return the output plus 2.
    //  **/
    // /**
function addTwo(x) {
    return x + 2;
}

console.log("Is the number 5 added by 2");
console.log(addTwo(5))

    //  * TODO:
    //  * Write a function named isMultipleOfTen(x).
    //  * This function should return a boolean value.
    //  **/
    // /**
function isMultipleOfTen(x) {
    return x % 10 === 0
}

console.log("Is 50 a multiple of ten?");
console.log(isMultipleOfTen(50))

    //  * TODO:
    //  * Write a function named isMultipleOftwo(x).
    //  * This function should return a boolean value.
    //  **/
    // /**
function isMultipleOfTwo(x) {
    return x % 2 === 0
}

console.log("is 222222222222 a multiple of 2?");
console.log(isMultipleOfTwo(222222222222))

    //  * TODO:
    //  * Write a function named isMultipleOftwoAndFour(x).
    //  * This function should return a boolean value.
    //  **/
    // /**
function isMultipleOfTwoAndFour(x) {
    return x % 2 === 0 && x % 4 === 0
}
console.log("is 9 a multiple of 2?");
console.log(isMultipleOfTwoAndFour(9))

    //  * TODO:
    //  * Write a function named isTrue(). This function should take in any input and return true
    //  * if the input provided is exactly equal to `true` in value and data type.
    //  **/
    // /**
function isTrue(x) {
    return x === true;
}
console.log("is the input provided exactly equal to 'true' in value and data type?");
console.log(isTrue(false))

    //  * TODO:
    //  * Write a function named isFalse(x). This function should take in a value and returns a true
    //  * if and only if the provided input is equal to false.
    //  **/
    // /**
function isFalse(x) {
    return x === false
}
console.log("is the input provided exactly equal to 'false' in value and data type?");
console.log(isFalse(false))

    //  * TODO:
    //  * Write a function named isVowel(x).
    //  * This function should return a boolean value.
    //  **/
    // /**
function isVowel(x) {
    return x === 'a' || x === 'e' || x === 'i' || x === 'o' || x === 'u';
}
console.log("is the letter g a vowel?");
console.log(isVowel('g'))
console.log("is the letter e a vowel?");
console.log(isVowel('e'))

    //  * TODO:
    //  * Write a function named triple(x).
    //  * This will return an input times 3.
    //  **/
    // /**
function triple(x) {
    return x * 3;
}
console.log("What is 4 tripled?");
console.log(triple(4))

    //  * TODO:
    //  * Write a function named QuadNum(x).
    //  * This will return a number times 4.
    //  **/
    // /**
function quadNum(x) {
    return x * 4
}
console.log("What is 300 quadrupled?");
console.log(quadNum(300))

    //  * TODO:
    //  * Write a function named modulus(a,b).
    //  * This will return the remainder when a / b.
    //  **/
    // /**

function modulus(a,b) {
    return a % b
}
console.log("What is the remainder of  50/3?");
console.log(modulus(50, 3));

    //  * TODO:
    //  * Write a function named degreesToRadians(x).
    //  * This function should convert from degrees to radians
    //  **/
    // /**
function degreesToRadians(x) {
    return x * Math.PI/180
}
console.log("What is 100 degrees in Radians?");
console.log(degreesToRadians(100))

    //  * TODO:
    //  * Write a function named absoluteValue(x).
    //  * This will return absolute value of a number.
    //  **/
    // /**
function absoluteValue(x) {
    return Math.abs(x)
}
console.log("What is the absolute value of -600?");
console.log(absoluteValue(-600))

    //  * TODO:
    //  * Write a function named reverseString(x).
    //  * This will return a string in reverse order.
    //  **/
function reverseString(x) {

    var splitString = x.split("")
    var reverseArray = splitString.reverse()
    var joinArray = reverseArray.join("")
    return joinArray;
}
console.log("What is 'Hello World! spelled backwards");
console.log(reverseString('Hello World!'))

/*
function reverseString(str) {
    // Step 1. Use the split() method to return a new array
    var splitString = str.split(""); // var splitString = "hello".split("");
    // ["h", "e", "l", "l", "o"]

    // Step 2. Use the reverse() method to reverse the new created array
    var reverseArray = splitString.reverse(); // var reverseArray = ["h", "e", "l", "l", "o"].reverse();
    // ["o", "l", "l", "e", "h"]

    // Step 3. Use the join() method to join all elements of the array into a string
    var joinArray = reverseArray.join(""); // var joinArray = ["o", "l", "l", "e", "h"].join("");
    // "olleh"

    //Step 4. Return the reversed string
    return joinArray; // "olleh"
}
reverseString("hello");

*/













